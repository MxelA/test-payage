﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WebApp.Dtos.Requests;
using WebApp.Dtos.Responses;
using WebApp.Repository;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizeController : ControllerBase
    {
        private readonly IRepositoryWrapper repository;
        private readonly IMapper mapper;

        public AuthorizeController(IRepositoryWrapper repository, IMapper mapper )
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        [HttpPost]
        public async Task<IActionResult> CreateAuthorize([FromBody] AuthorizePaymentRequest request)
        {
            var payment  = await repository.Payment.AuthorizePayment(request);

            return Ok(mapper.Map<PaymentCreateResponse>(payment));
        }

        [HttpGet]
        public async Task<IActionResult> GetAuthorize([FromQuery] PaginationParameterRequest paginationParameter)
        {
            var payments = repository.Payment.Paginate(paginationParameter);

            return Ok(mapper.Map<PaginateListResource<PaymentResponse>>(payments));
        }

        [HttpPost("{id}/capture")]
        public async Task<IActionResult> AuthorizeCapture(Guid id,[FromBody] AuthorizeCaptureRequest request)
        {
           
            var payment = await repository.Payment.AuthorizeCapture(id, request);

            return Ok(mapper.Map<PaymentCreateResponse>(payment));
        }

        [HttpPost("{id}/voids")]
        public async Task<IActionResult> AuthorizeVoids(Guid id, [FromBody] AuthorizeCaptureRequest request)
        {

            var payment = await repository.Payment.AuthorizeVoids(id, request);

            return Ok(mapper.Map<PaymentCreateResponse>(payment));
        }
    }
}
