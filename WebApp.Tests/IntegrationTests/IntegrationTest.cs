﻿using Microsoft.AspNetCore.Mvc.Testing;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebApp.Response;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using WebAppDB;

namespace WebApp.Tests
{
    public class IntegrationTest
    {
        protected readonly HttpClient TestClient;
        protected bool seed = false;



        protected IntegrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    //builder.UseEnvironment("Test");
                    builder.ConfigureServices(services =>
                    {
                        services.RemoveAll(typeof(AppDbContext));

                        services.AddDbContext<AppDbContext>(options =>
                        {
                            options.UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString());
                            //options.UseSqlServer("Server=localhost;User Id=sa;Password=wjwotnbp1;Database=web_app_test;Trusted_Connection=False;");
                        });

                        var sp = services.BuildServiceProvider();
                        using (var scope = sp.CreateScope())
                        {
                            var scopedServices = scope.ServiceProvider;
                            var db = scopedServices.GetRequiredService<AppDbContext>();
                            //db.Database.EnsureDeleted();
                            db.Database.EnsureCreated();
                            
                            try
                            {
                                //Add seeders

                            } catch(Exception e) {

                            }



                            //VehicleModelSeeder.SeedOverApplicationDbContext(context);
                        }
                    });

                });
            TestClient = appFactory.CreateClient();
        }
    }
}
