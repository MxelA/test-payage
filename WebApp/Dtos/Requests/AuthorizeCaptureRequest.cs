﻿using System;
using System.ComponentModel.DataAnnotations;
using WebApp.Validators;

namespace WebApp.Dtos.Requests
{
    public class AuthorizeCaptureRequest
    {
        [Required]
        [MaxLength(50)]
        public string OrderReference { get; set; }
    }
}
