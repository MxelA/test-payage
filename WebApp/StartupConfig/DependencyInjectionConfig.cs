using Microsoft.Extensions.DependencyInjection;
using WebApp.Repository;
using Microsoft.AspNetCore.Http;
using WebApp.StateMachine;

namespace WebApp.StartupConfig
{
    public static class DependencyInjectionConfig
    {
        
        public static void DependencyInjectionServices(this IServiceCollection services)
        {
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>(); 
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            services.AddScoped<AuthorizeStateMachine>();
        }
    }
}