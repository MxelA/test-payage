﻿using System;
using System.ComponentModel.DataAnnotations;
using WebApp.Validators;

namespace WebApp.Dtos.Requests
{
    public class AuthorizePaymentRequest
    {
        [Required]
        public decimal Amount { get; set; }

        [Required]
        [MaxLength(3)]
        public string Currency { get; set; }
        [Required]
        [MaxLength(16)]
        [MinLength(16)]
        public string CardHolderNumber { get; set; }
        [Required]
        [MaxLength(200)]
        public string HolderName { get; set; }
        [Required]
        [Range(1, 12)]
        public int ExpirationMonth { get; set; }
        [Required]
        [ExpirationYearValidator]
        public int ExpirationYear { get; set; }
        [Required]
        [Range(100, 999)]
        public int Cvv { get; set; }
        [Required]
        [MaxLength(50)]
        public string OrderReference { get; set; }
    }
}
