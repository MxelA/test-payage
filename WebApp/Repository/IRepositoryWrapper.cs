using System.Threading.Tasks;
using WebApp.Repository.Payment;

namespace WebApp.Repository
{
    public interface IRepositoryWrapper
    {  
        void Save();
        Task SaveAsync();

        IPaymentRepository Payment { get; }
        
    }
}