﻿using AutoMapper;
using Automatonymous;
using System;
using System.Threading.Tasks;
using WebApp.Dtos.Requests;
using WebApp.StateMachine;
using WebAppDB;
using WebAppDB.Enums;
using PaymentEntity = WebAppDB.DbEntities.Payment;
namespace WebApp.Repository.Payment
{
    public class PaymentRepository: RepositoryBase<PaymentEntity>, IPaymentRepository
    {
        private readonly AppDbContext dbContext;
        private readonly IRepositoryWrapper repository;
        private readonly IMapper mapper;
        private readonly AuthorizeStateMachine authorizeStateMachine;

        public PaymentRepository(AppDbContext dbContext, IRepositoryWrapper repository, IMapper mapper, AuthorizeStateMachine authorizeStateMachine): base(dbContext)
        {
            this.dbContext = dbContext;
            this.repository = repository;
            this.mapper = mapper;
            this.authorizeStateMachine = authorizeStateMachine;
        }

        public async Task<PaymentEntity> AuthorizePayment(AuthorizePaymentRequest request)
        {
           
            var payment = mapper.Map<AuthorizePaymentRequest, PaymentEntity>(request);
            await authorizeStateMachine.RaiseEvent(payment, authorizeStateMachine.ProcessingAuthorizePayment);

            payment.Status = PaymentStatusEnum.Authorized;

            await repository.Payment.CreateAsync(payment);
            await repository.SaveAsync();

           
            return payment;
        }

        public async Task<PaymentEntity> AuthorizeCapture(Guid paymentId, AuthorizeCaptureRequest request)
        {
            var payment = await repository.Payment.ByCondition(p => p.Id.Equals(paymentId)).GetFirstAsync();
            payment.Status = PaymentStatusEnum.Captured;

            await authorizeStateMachine.RaiseEvent(payment, authorizeStateMachine.ProcessingCapturedPayment);

            repository.Payment.Update(payment);
            await repository.SaveAsync();

            return payment;
        }
        
        public async Task<PaymentEntity> AuthorizeVoids(Guid paymentId, AuthorizeCaptureRequest request)
        {
            var payment = await repository.Payment.ByCondition(p => p.Id.Equals(paymentId)).GetFirstAsync();

            payment.Status = PaymentStatusEnum.Voided;
            await authorizeStateMachine.RaiseEvent(payment, authorizeStateMachine.ProcessingVoidedPayment);

            repository.Payment.Update(payment);
            await repository.SaveAsync();

            return payment;
        }
    }
}
