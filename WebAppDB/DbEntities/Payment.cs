﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebAppDB.Enums;

namespace WebAppDB.DbEntities
{
    public class Payment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        [MaxLength(3)]
        public string Currency { get; set; }
        [Required]
        [MaxLength(100)]
        public string State { get; set; }
        [Required]
        public PaymentStatusEnum Status { get; set; }
        [Required]
        [MaxLength(16)]
        public string CardHolderNumber { get; set; }
        [Required]
        [MaxLength(200)]
        public string HolderName { get; set; }
        [Required]
        public int ExpirationMonth { get; set; }
        [Required]
        public int ExpirationYear { get; set; }
        [Required]
        public int Cvv { get; set; }
        [Required]
        [MaxLength(50)]
        public string OrderReference { get; set; }

    }
}
