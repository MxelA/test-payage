﻿using System;
using WebAppDB.Enums;

namespace WebApp.Dtos.Responses
{
    public class PaymentCreateResponse
    {
        public Guid Id { get; set; }
        public PaymentStatusEnum Status { get; set; }
    }
}
