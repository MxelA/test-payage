namespace WebApp.Dtos.Requests
{
    public class PaginationParameterRequest
{
	const int maxPageSize = 100;
	private int _pageNumber = 1;
	public int PageNumber { 
		get {
			return _pageNumber;
		}
		set{
			_pageNumber = (value < 1) ? _pageNumber : value;
		} 
	}

	private int _pageSize = 10;
	public int PageSize
	{
		get
		{
			return _pageSize;
		}
		set
		{
			_pageSize = (value > maxPageSize) ? maxPageSize : value;
		}
	}
}
}