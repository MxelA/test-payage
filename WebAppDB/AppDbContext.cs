using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebAppDB.DbEntities;

namespace WebAppDB
{
    public class AppDbContext: DbContext
    {
        public IConfiguration Configuration { get; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Payment> Payments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payment>().Property(c => c.Amount).HasColumnType("decimal(18,4)").IsRequired(true);
        }
    }
}