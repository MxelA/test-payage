# Test - Payage

## Docker
``` 
# MSSQl Server
docker-compose up -d
```

## Build Setup
``` CLI
# install dependencies
dotnet restore

#build application
dotnet build

#migrate DB 
dotnet ef database update -s .\WebApp\WebApp.csproj

# run server at localhost:5000
dotnet run
```

## API documentation
``` SWAGGER
# open URL
localhost:5000/swagger
```

For detailed explanation on how things work, checkout the [Microsoft Documentation](https://docs.microsoft.com/en-us/dotnet/fundamentals)
