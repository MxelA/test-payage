﻿using System;
using System.Threading.Tasks;
using WebApp.Dtos.Requests;
using PaymentEntity = WebAppDB.DbEntities.Payment;

namespace WebApp.Repository.Payment
{
    public interface IPaymentRepository: IRepositoryBase<PaymentEntity>
    {
        Task<PaymentEntity> AuthorizePayment(AuthorizePaymentRequest request);
        Task<PaymentEntity> AuthorizeCapture(Guid paymentId, AuthorizeCaptureRequest request);
        Task<PaymentEntity> AuthorizeVoids(Guid paymentId, AuthorizeCaptureRequest request);
    }
}
