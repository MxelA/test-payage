using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using WebAppDB;
using WebApp.Repository.Payment;
using AutoMapper;
using WebApp.StateMachine;

namespace WebApp.Repository
{
    public class RepositoryWrapper: IRepositoryWrapper
    {
        private AppDbContext _applicationDbContext;
        
        private readonly IMapper mapper;
        private readonly AuthorizeStateMachine authorizeStateMachine;

        // private IAuthRepository _auth;

        public RepositoryWrapper(
            AppDbContext applicationContext,
            IMapper mapper,
            AuthorizeStateMachine authorizeStateMachine 
        ){
            _applicationDbContext     = applicationContext;
            this.mapper = mapper;
            this.authorizeStateMachine = authorizeStateMachine;
        }       

        public void Save()
        {
            _applicationDbContext.SaveChanges();
        }

        public async Task SaveAsync()
        {
           await _applicationDbContext.SaveChangesAsync();
        }

        private IPaymentRepository _payment;
        public IPaymentRepository Payment
        {
            get
            {
                if (_payment == null)
                {
                    _payment = new PaymentRepository(_applicationDbContext, this, mapper, authorizeStateMachine);
                }

                return _payment;
            }
        }
    }
}