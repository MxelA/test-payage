﻿using Newtonsoft.Json;
using System;
using WebAppDB.Enums;

namespace WebApp.Dtos.Responses
{
    public class PaymentResponse
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string HolderName { get; set; }
        [JsonIgnore]
        public string CardHolderNumber { get; set; }
        [JsonProperty(PropertyName = "CardHolderNumber")]
        public string CardHolderNumberAnonymization
        { 
            get { 
                return CardHolderNumber.Remove(6, 6).Insert(6, "******");
            } 
        }
        public PaymentStatusEnum Status { get; set; }
    }
}
