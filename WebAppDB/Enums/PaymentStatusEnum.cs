﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebAppDB.Enums
{
    public enum PaymentStatusEnum
    {
        Authorized = 1,
        Captured,
        Voided
    }
}
