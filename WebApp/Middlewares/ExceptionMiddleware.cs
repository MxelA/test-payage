using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using WebApp.Exceptions;
using Microsoft.AspNetCore.Http;

namespace WebApp.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
 
        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }
    
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (HttpException e) {
                await HandleHttpException(httpContext, e);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private Task HandleHttpException(HttpContext context, HttpException exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = exception.StatusCode;
    
            return context.Response.WriteAsync(
                JsonSerializer.Serialize(
                       new {
                        Success = false,
                        Message = exception.Message
                    }
                )
            );
        }
    
        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
    
            return context.Response.WriteAsync(
                JsonSerializer.Serialize(
                       new {
                        Success = false,
                        Message = exception.Message
                    }
                )
            );
        }
    }
}