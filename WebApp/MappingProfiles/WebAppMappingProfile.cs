﻿using AutoMapper;
using WebApp.Dtos.Requests;
using WebApp.Dtos.Responses;
using WebApp.Response;
using WebAppDB.DbEntities;

namespace WebApp.MappingProfiles
{
    public class WebAppMappingProfile : Profile
    {
        public WebAppMappingProfile()
        {
            CreateMap<Payment, PaymentCreateResponse>();
            CreateMap<AuthorizePaymentRequest, Payment>();
            CreateMap<ServicePaginateList<Payment>, PaginateListResource<PaymentResponse>>();
            CreateMap<Payment, PaymentResponse>();
        }
    }
}
