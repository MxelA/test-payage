﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Validators
{
    public class ExpirationYearValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var currentYear = DateTime.Now.Year % 100;
            var expirationYear = (int)value;
            if (expirationYear < currentYear)
            {
                return new ValidationResult(GetErrorMessage(expirationYear));
            }

            return ValidationResult.Success;
        }

        public string GetErrorMessage(int year)
        {
            return $"ExpirationYear {year} is not valid";
        }
    }
}
