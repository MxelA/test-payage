using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApp.Dtos.Requests;
using WebApp.Response;

namespace WebApp.Repository
{
    public interface IRepositoryBase<T> where T: class
    {
        IQueryable<T> GetNewIQueryableModel();
        IQueryable<T> GetIQueryableModel();
        IList<T> Get();
        T GetFirst();
        T GetFirstOrFail();
        Task<IList<T>> GetAsync();
        Task<T> GetFirstAsync();
        Task<T> GetFirstOrFailAsync();
        IRepositoryBase<T> ByCondition(Expression<Func<T, bool>> expression);
        IRepositoryBase<T> WithTrashData();
        IRepositoryBase<T> Include(string parametar);
        ServicePaginateList<T> Paginate(PaginationParameterRequest paginationParameter);
        void Create(T entity);
        Task CreateAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}